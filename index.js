const collection = [
  {
    name: "test 1",
    rarity: "rare",
    color: "red",
  },
  {
    name: "test 2",
    rarity: "rare",
    color: "black",
  },
  {
    name: "test 3",
    rarity: "common",
    color: "green",
  },
];

const myFilter = (collection) => (key) => (val) =>
  collection.filter((item) => item[key] === val);

const myColorFilter = myFilter(collection)("color");
const myRarityFilter = myFilter(collection)("rarity");

console.log("myFilter: ", myFilter(collection)("rarity")("rare"));
console.log("myFilter: ", myFilter(collection)("color")("red"));
console.log("myColorFilter: ", myColorFilter("green"));
console.log("myRarityFilter: ", myRarityFilter("rare"));

console.log(
  "myRarityAndColorFilter: ",
  myFilter(myRarityFilter("rare"))("color")("black")
);
